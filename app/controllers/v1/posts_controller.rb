# frozen_string_literal: true

module V1
  class PostsController < ApplicationController
    def pendings
      pending_collection = Post.pending.order(created_at: :asc)
      render_serialized_response(pending_collection, V1::PostSerializer)
    end

    def show
      post = Post.find_by_id(params[:id])

      if post
        render_serialized_response(post, V1::PostSerializer)
      else
        render_not_found_response
      end
    end
  end
end
