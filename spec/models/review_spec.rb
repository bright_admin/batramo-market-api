# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Review, type: :model do
  context 'belongs to reviewee' do
    let(:reviewee) { FactoryBot.create(:user) }
    let(:review) { FactoryBot.create(:review, reviewee: reviewee) }

    it 'returns user' do
      expect(review.reviewee).to eq(reviewee)
    end
  end

  context 'belongs to reviewer' do
    let(:reviewer) { FactoryBot.create(:user) }
    let(:review) { FactoryBot.create(:review, reviewer: reviewer) }

    it 'returns user' do
      expect(review.reviewer).to eq(reviewer)
    end
  end
end
