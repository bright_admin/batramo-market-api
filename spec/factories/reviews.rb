# frozen_string_literal: true

FactoryBot.define do
  factory :review do
    reviewer_id { FactoryBot.create(:user).id }
    reviewee_id { FactoryBot.create(:user).id }
    body { Faker::Lorem.paragraph(2) }
    rating { rand(1..5).to_i }
  end
end
