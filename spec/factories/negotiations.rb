# frozen_string_literal: true

FactoryBot.define do
  factory :negotiation do
    association :post, factory: :post
    association :buyer, factory: :user
    association :seller, factory: :user
  end
end
