# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::PostSearchController, type: :routing do
  describe 'GET #search' do
    it 'routes correctly' do
      expect(get: 'v1/post_search/search').to route_to('v1/post_search#search')
    end
  end
end
