# frozen_string_literal: true

class CreateNegotiations < ActiveRecord::Migration[5.2]
  def change
    create_table :negotiations do |t|
      t.belongs_to :post
      t.belongs_to :buyer
      t.belongs_to :seller
      t.timestamps
    end
  end
end
