# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::ReviewsController, type: :routing do
  describe 'GET #index' do
    it 'route correctly' do
      expect(get: '/v1/users/1/reviews').to route_to('v1/reviews#index', user_id: '1')
    end
  end

  describe 'POST #create' do
    it 'routes correctly' do
      expect(post: '/v1/reviews').to route_to('v1/reviews#create')
    end
  end
end
