# frozen_string_literal: true

module V1
  class ReviewsController < ApplicationController
    before_action :authenticate_user!, only: %i[create]

    def index
      reviews = Review.where(reviewee_id: params[:user_id])
      render json: reviews, each_serializer: V1::ReviewsSerializer, status: :ok
    end

    def create
      review = Review.new(review_params)

      if review.save
        render json: review, serializer: V1::ReviewsSerializer, status: :ok
      else
        error_occured_response(review)
      end
    end

    private

    def review_params
      params.require(:review).permit(:reviewee_id, :reviewer_id, :body, :rating)
    end
  end
end
