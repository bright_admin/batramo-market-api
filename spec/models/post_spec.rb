# frozen_string_literal: true

require 'rails_helper'
require 'aasm/rspec'

RSpec.describe Post, type: :model do
  it 'has valid bot' do
    expect(FactoryBot.build(:post)).to be_valid
  end

  context 'belongs to user' do
    let(:user) { FactoryBot.create(:user) }
    let(:post) { FactoryBot.create(:post, user: user) }

    it 'returns user' do
      expect(post.user).to eq(user)
    end
  end

  describe 'state_machine for sale_status' do
    [
      { method: 'sell', from: 'pending', to: 'sold' },
      { method: 'reserve', from: 'pending', to: 'reserved' }
    ].each do |transition|
      context transition[:method] do
        let(:post) { FactoryBot.create(:post, sale_status: transition[:from]) }

        it "transitions from #{transition[:from]} to #{transition[:to]}" do
          expect(post).to transition_from(transition[:from]).to(transition[:to]).on_event(transition[:method])
        end
      end
    end
  end
end
