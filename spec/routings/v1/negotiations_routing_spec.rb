# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::NegotiationsController, type: :controller do
  describe 'GET #index' do
    it 'routes correctly' do
      expect(get: '/v1/negotiations').to route_to('v1/negotiations#index')
    end
  end

  describe 'POST #create' do
    it 'routes correctly' do
      expect(post: '/v1/negotiations').to route_to('v1/negotiations#create')
    end
  end

  describe 'DELETE #destroy' do
    it 'routes correctly' do
      expect(delete: '/v1/negotiations/1').to route_to('v1/negotiations#destroy', id: '1')
    end
  end
end
