# frozen_string_literal: true

FactoryBot.define do
  factory :miscellaneous_detail do
    price { Faker::Commerce.price }
    title { Faker::Commerce.product_name }
    description { Faker::Lorem.sentence(3, true, 3) }
  end
end
