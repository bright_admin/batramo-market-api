# frozen_string_literal: true

module RecordErrors
  def record_errors(record)
    record.errors.full_messages
  end
end
