# frozen_string_literal: true

module V1
  module Sellers
    class BaseController < ApplicationController
      before_action :authenticate_user!
    end
  end
end
