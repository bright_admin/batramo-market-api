# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    association :sender, factory: :user
    association :receiver, factory: :user
    body { Faker::Lorem.sentence(3) }
  end
end
