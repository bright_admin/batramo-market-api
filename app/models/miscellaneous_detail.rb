# frozen_string_literal: true

class MiscellaneousDetail < ApplicationRecord
  has_many :posts, as: :detail
end
