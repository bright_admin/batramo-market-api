# frozen_string_literal: true

module V1
  class FavoriteSerializer < ActiveModel::Serializer
    attributes :id
    belongs_to :post
    class PostSerializer < ActiveModel::Serializer
      attributes :id
    end
  end
end
