# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'authenticate_user' do |user|
  before do
    logout(user) if user
    post :create, params: { post_id: 1 }
  end

  it 'returns status 401' do
    expect(response.status).to eq(401)
  end
end

RSpec.describe V1::FavoritesController, type: :controller do
  describe 'index' do
    include_examples 'authenticate_user'

    context 'renders successfully' do
      let(:current_user) { FactoryBot.create(:user) }

      let!(:favorites) { FactoryBot.create_list(:favorite, 5, user: current_user) }

      before do
        login(current_user)
        get :index
      end

      it 'renders status 200' do
        expect(response.status).to eq(200)
      end

      it 'renders serialized favorites' do
        expect(json).to eq(active_model_serialize(favorites, V1::FavoriteSerializer))
      end
    end
  end

  describe 'create' do
    include_examples 'authenticate_user'

    context 'success favorite' do
      let(:current_user) { FactoryBot.create(:user) }

      before do
        login current_user
      end

      let(:_post) { FactoryBot.create(:post) }

      before do
        post :create, params: { post_id: _post.id }
      end

      it 'create new favorite' do
        expect do
          post :create, params: { post_id: _post.id }
        end.to change(Favorite, :count).by(1)
      end

      it 'renders status 200' do
        expect(response.status).to eq(200)
      end

      it 'renders serialized favorite' do
        expect(json).to eq(active_model_serialize(Favorite.last, V1::FavoriteSerializer))
      end
    end

    context 'error occurs' do
      let(:current_user) { FactoryBot.create(:user) }
      let(:_post) { FactoryBot.create(:post) }

      before do
        login current_user
        post :create, params: { post_id: 0 }
      end

      it 'status 422' do
        expect(response.status).to eq(422)
      end

      it 'json to have errors' do
        expect(json['errors']).to eq(['Post must exist'])
      end
    end
  end

  describe 'DELETE #destroy' do
    include_examples 'authenticate_user'

    let(:current_user) { FactoryBot.create(:user) }

    before { login current_user }

    context 'favorite unsuccesful' do
      before do
        delete :destroy, params: { id: 0 }
      end

      it 'return status 422' do
        expect(response.status).to eq(422)
      end
    end

    context 'successful' do
      let(:user) { FactoryBot.create(:user) }
      let!(:favorite) { FactoryBot.create(:favorite, user: user) }

      before do
        login user
      end

      it 'returns status 200' do
        delete :destroy, params: { id: favorite.id }
        expect(response.status).to eq(200)
      end

      it 'destroysa favorite' do
        expect do
          delete :destroy, params: { id: favorite.id }
        end.to change(Favorite, :count).by(-1)
      end
    end
  end
end
