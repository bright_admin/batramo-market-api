# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::CategoriesController, type: :controller do
  describe 'index' do
    let(:categories) { YAML.load_file('config/categories/categories.yml') }
    before do
      get(:index)
    end

    it 'renders json of categories' do
      expect(json).to eq(categories)
    end

    it 'renders status 200' do
      expect(response.status).to eq(200)
    end
  end
end
