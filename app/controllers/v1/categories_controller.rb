# frozen_string_literal: true

module V1
  class CategoriesController < ApplicationController
    def index
      categories = YAML.load_file('config/categories/categories.yml')
      render json: categories, status: :ok
    end
  end
end
