# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Notification, type: :model do
  it 'has valid factory' do
    expect(FactoryBot.build(:notification)).to be_valid
  end

  describe 'associations' do
    %w[reciever sender].each do |user_type|
      let(user_type.to_sym) { FactoryBot.create(:user) }
    end

    let(:post) { FactoryBot.create(:post) }
    let(:notification) do
      FactoryBot.create(:notification, reciever: reciever, sender: sender, notifiable: post)
    end

    it 'belongs to sender' do
      expect(notification.sender).to eq(sender)
    end

    it 'belongs to receiver' do
      expect(notification.reciever).to eq(reciever)
    end

    it 'belongs to post as notifiable' do
      expect(notification.notifiable).to eq(post)
    end
  end
end
