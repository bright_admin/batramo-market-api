# frozen_string_literal: true

module V1
  class NegotiationsController < ApplicationController
    before_action :authenticate_user!

    def index
      negotiations = Negotiation.involving(current_user.id)
      render json: negotiations, each_serializer: V1::NegotiationSerializer, status: :ok
    end

    def create
      negotiation = Negotiation.new(negotiation_params)

      if negotiation.save
        render json: negotiation, serializer: V1::NegotiationSerializer, status: :ok
      else
        error_occured_response(negotiation)
      end
    end

    def destroy
      negotiation = Negotiation.find_by(id: params[:id])

      return error_occured_response unless negotiation&.involves?(current_user)

      negotiation.destroy
      render json: { message: 'Deleted successfully' }, status: :ok
    end

    private

    def negotiation_params
      params.require(:negotiation).permit(:buyer_id, :seller_id, :post_id)
    end
  end
end
