# frozen_string_literal: true

module V1
  module Sellers
    class PostsController < Sellers::BaseController
      before_action :find_post!, only: %i[sell reserve]

      %w[pending reserved sold].each do |action|
        # def pending
        # def reserve
        # def sold
        define_method(action) do
          collection = current_user.posts.send(action)
          render_serialized_response(collection, V1::PostSerializer)
        end
      end

      %w[sell reserve].each do |action|
        # def sell
        # def reserve
        define_method(action) do
          if @post.send("#{action}!") # @post.sell! or @post.reserve!
            render_serialized_response(@post, V1::PostSerializer)
          else
            render json: { error: "Unable to #{action}" }, status: :unprocessable_entity
          end
        end
      end

      private

      def find_post!
        @post = current_user.posts.find_by_id(params[:id])
        render_not_found_response unless @post
      end
    end
  end
end
