# frozen_string_literal: true

module V1
  class PostSearchController < ApplicationController
    def search
      @post = Post.search(params[:query])
      render json: @post, each_serializer: V1::PostSerializer
    end
  end
end
