# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::Sellers::PostsController, type: :controller do
  %w[pending reserved sold].each do |action|
    describe "GET ##{action}" do
      let(:current_user) { FactoryBot.create(:user) }

      before do
        login(current_user)
      end

      context 'renders serialized posts' do
        let(:serialized_collection) { active_model_serialize(collection, V1::PostSerializer) }
        let!(:collection) { FactoryBot.create_list(:post, 5, sale_status: action, user: current_user) }

        before do
          get(action.to_sym)
        end

        it "renders #{action} posts" do
          expect(json).to eq(serialized_collection)
        end
      end

      context 'renders posts only belonging to current_user' do
        let(:posts) { FactoryBot.create_list(:post, 5, sale_status: action) }
        before do
          get(action.to_sym)
        end

        it 'renders empty array' do
          expect(json).to be_empty
        end
      end
    end
  end

  describe 'POST #sell' do
    let(:current_user) { FactoryBot.create(:user) }

    before do
      login(current_user)
    end

    context 'post not found' do
      before do
        post(:sell, params: { id: 0 })
      end

      it 'renders status 404(not_found)' do
        expect(response.status).to eq(404)
      end
    end

    context 'sell post' do
      let(:_post) { FactoryBot.create(:post, user: current_user) }

      before do
        post(:sell, params: { id: _post.id })
      end

      it 'changes status from pending to sold' do
        expect(_post.reload).to be_sold
      end

      it 'renders response status 200(ok)' do
        expect(response.status).to eq(200)
      end

      pending 'sends mail'
      pending 'creates notification'
    end

    context 'invalid transition' do
      let(:_post) { FactoryBot.create(:post, user: current_user, sale_status: :sold) }

      before do
        post(:sell, params: { id: _post.id })
      end

      it 'response with status 422(unprocessable_entity)' do
        expect(response.status).to eq(422)
      end

      it 'renders json with error message' do
        expect(json['error']).to eq('Unable to sell')
      end
    end
  end
end
