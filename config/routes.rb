# frozen_string_literal: true

Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'

  namespace :v1 do
    scope module: :sellers, path: :sellers do
      resources :posts, only: [] do
        collection do
          %i[pending sold reserved].each do |action|
            get action
          end
        end
        member do
          post :sell
          post :reserve
        end
      end
    end

    resources :posts, only: %i[show] do
      collection do
        get :pendings
      end
      resources :favorites, only: %i[create]
    end

    resources :users, only: [] do
      resources :reviews, only: %i[index]
    end

    resources :negotiations, only: %i[index create destroy] do
      resources :messages, only: %i[index create]
    end
    resources :reviews, only: %i[create destroy]
    resources :favorites, only: %i[destroy index]
    resources :categories, only: %i[index]
    resources :post_search, only: [] do
      collection { get :search }
    end
  end
end
