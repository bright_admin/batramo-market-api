# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::FavoritesController, type: :routing do
  describe 'POST #create' do
    it 'route correctly' do
      expect(post: '/v1/posts/1/favorites').to route_to('v1/favorites#create', post_id: '1')
    end
  end

  describe 'DELETE #destroy' do
    it 'routes correctly' do
      expect(delete: '/v1/favorites/1').to route_to('v1/favorites#destroy', id: '1')
    end
  end

  describe 'GET #index' do
    it 'routes correctly' do
      expect(get: '/v1/favorites').to route_to('v1/favorites#index')
    end
  end
end
