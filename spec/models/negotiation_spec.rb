# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Negotiation, type: :model do
  it 'has valid factory' do
    expect(FactoryBot.build(:negotiation)).to be_valid
  end

  describe '#involves?' do
    subject(:outcome) { negotiation.involves?(user) }

    context 'user involved' do
      let(:user) { FactoryBot.create(:user) }
      let(:negotiation) { FactoryBot.create(:negotiation, buyer: user) }

      it 'returns true' do
        expect(outcome).to be_truthy
      end
    end

    context 'user not invovled' do
      let(:user) { FactoryBot.create(:user) }
      let(:negotiation) { FactoryBot.create(:negotiation) }

      it 'returns false' do
        expect(outcome).to be_falsey
      end
    end
  end

  context 'duplicate of buyer negotiation per product' do
    let(:post) { FactoryBot.create(:post) }
    let(:buyer) { FactoryBot.create(:user) }
    let!(:negotiation) { FactoryBot.create(:negotiation, post: post, buyer: buyer) }

    subject(:control_negotiation) { FactoryBot.build(:negotiation, buyer: buyer, post: post) }

    it 'is invalid' do
      expect(control_negotiation).not_to be_valid
    end
  end

  describe 'involving' do
    let(:user) { FactoryBot.create(:user) }
    context 'user is buyer' do
      let(:negotiations) { FactoryBot.create_list(:negotiation, 5, buyer: user) }

      it 'includes negotiations' do
        expect(Negotiation.involving(user)).to include(*negotiations)
      end
    end

    context 'user is seller' do
      let(:negotiations) { FactoryBot.create(:negotiation, seller: user) }

      it 'includes negotiations' do
        expect(Negotiation.involving(user)).to include(*negotiations)
      end
    end

    context 'user not involved' do
      let(:negotiations) { FactoryBot.create(:negotiation) }

      it 'does not include negotiations' do
        expect(Negotiation.involving(user)).not_to include(*negotiations)
      end
    end
  end
end
