# frozen_string_literal: true

class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.boolean    :read
      t.text       :body
      t.belongs_to :sender
      t.belongs_to :receiver
      t.belongs_to :negotiation
      t.timestamps
    end
  end
end
