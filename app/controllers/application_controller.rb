# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ResponseHelper
  include DeviseTokenAuth::Concerns::SetUserByToken
end
