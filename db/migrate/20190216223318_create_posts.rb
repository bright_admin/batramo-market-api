# frozen_string_literal: true

class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.integer  :super_type, default: 0
      t.integer  :sale_status, default: 0
      t.boolean  :price_negotiable, default: false
      t.belongs_to :user
      t.references :detail, polymorphic: true, index: true
      t.timestamps
    end
  end
end
