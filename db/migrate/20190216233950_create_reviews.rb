# frozen_string_literal: true

class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.integer    :rating
      t.text       :body
      t.belongs_to :reviewer
      t.belongs_to :reviewee
      t.timestamps
    end
  end
end
