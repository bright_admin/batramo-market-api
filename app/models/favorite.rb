# frozen_string_literal: true

class Favorite < ApplicationRecord
  belongs_to :post, required: true
  belongs_to :user
end
