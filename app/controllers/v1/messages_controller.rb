# frozen_string_literal: true

module V1
  class MessagesController < ApplicationController
    before_action :authenticate_user!
    before_action :find_negotiation!, only: %i[index create]

    def index
      render_serialized_response(@negotiation.messages, V1::MessageSerializer)
    end

    def create
      message = @negotiation.messages.build(message_params)

      if message.save
        render_serialized_response(message, V1::MessageSerializer)
      else
        error_occured_response(message)
      end
    end

    private

    def find_negotiation!
      @negotiation ||= Negotiation.find_by(id: params[:negotiation_id])
      return render_not_found_response unless @negotiation
    end

    def message_params
      params.require(:message).permit(:body, :sender_id, :receiver_id)
    end
  end
end
