# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    association :reciever, factory: :user
    association :sender, factory: :user
  end
end
