# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::PostSearchController, type: :controller do
  describe 'GET #search' do
    let!(:posts) { FactoryBot.create_list(:post, 5) }
    let!(:amazing_post) { FactoryBot.create(:post, detail: FactoryBot.create(:miscellaneous_detail, title: 'Amazing product')) }

    let(:serialized_post) { active_model_serialize(amazing_post, V1::PostSerializer) }

    before do
      Post.reindex
      get :search, params: { query: 'amazing' }
    end

    it 'includes products with keyword' do
      expect(json).to include(serialized_post)
    end

    it 'render status 200(ok)' do
      expect(response.status).to eq(200)
    end
  end
end
