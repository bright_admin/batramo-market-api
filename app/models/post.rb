# frozen_string_literal: true

class Post < ApplicationRecord
  include AASM
  include Posts::StateMachine # /models/concerns/posts/state_machine
  include Posts::SearchKickData # /models/concerns/posts/search_kick_data

  searchkick

  belongs_to :user
  belongs_to :detail, polymorphic: true

  enum super_type:  { miscellaneous: 0 }
  enum sale_status: { pending: 0, reserved: 1, sold: 2 }
end
