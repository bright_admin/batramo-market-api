# frozen_string_literal: true

module Posts
  module SearchKickData
    def search_data
      attributes.merge(
        title: detail.title,
        category: detail.category
      )
    end
  end
end
