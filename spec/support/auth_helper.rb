# frozen_string_literal: true

module AuthHelpers
  def login(user)
    auth_headers = user.create_new_auth_token
    request.headers.merge!(auth_headers)
  end

  def logout(_user)
    request.headers.merge!(client_id: nil, last_token: nil)
  end
end
