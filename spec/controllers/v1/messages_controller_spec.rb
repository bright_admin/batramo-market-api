# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::MessagesController, type: :controller do
  describe 'index' do
    let(:current_user) { FactoryBot.create(:user) }

    before do
      login current_user
    end

    context 'negotiation not found' do
      before do
        get :index, params: { negotiation_id: 0 }
      end

      it 'render status 404(not_found)' do
        expect(response.status).to eq(404)
      end
    end

    context 'renders success' do
      subject(:serialized_messages) { active_model_serialize(messages, V1::MessageSerializer) }

      let(:negotiation) { FactoryBot.create(:negotiation, buyer: current_user) }
      let!(:messages) { FactoryBot.create_list(:message, 5, negotiation: negotiation) }

      before do
        get :index, params: { negotiation_id: negotiation.id }
      end

      it 'render status 200(ok)' do
        expect(response.status).to eq(200)
      end

      it 'render serialized messages' do
        expect(json).to eq(serialized_messages)
      end
    end
  end

  describe 'create' do
    let(:current_user) { FactoryBot.create(:user) }

    before do
      login current_user
    end

    context 'negotiation not found' do
      before do
        post :create, params: { negotiation_id: 0 }
      end

      it 'render status 404(not_found)' do
        expect(response.status).to eq(404)
      end
    end

    context 'renders success' do
      subject(:serialized_messages) { active_model_serialize(messages, V1::MessageSerializer) }

      let(:message_valid_params) do
        FactoryBot.attributes_for(:message,
                                  sender_id: FactoryBot.create(:user).id,
                                  receiver_id: FactoryBot.create(:user).id)
      end

      let(:negotiation) { FactoryBot.create(:negotiation, buyer: current_user) }

      before do
        post :create, params: { negotiation_id: negotiation.id, message: message_valid_params }
      end

      it 'render status 200(ok)' do
        expect(response.status).to eq(200)
      end

      it 'creates a new message' do
        expect do
          post :create, params: { negotiation_id: negotiation.id, message: message_valid_params }
        end.to change(negotiation.messages, :count).by(1)
      end
    end
  end
end
