# frozen_string_literal: true

module ResponseHelper
  include RecordErrors

  def render_serialized_response(record, serializer)
    if record.respond_to?(:each)
      render json: record, each_serializer: serializer, status: :ok
    else
      render json: record, serializer: serializer, status: :ok
    end
  end

  def error_occured_response(record = nil)
    if record
      render json: { errors: record_errors(record) }, status: :unprocessable_entity
    else
      render json: { error: 'Error ocurred' }, status: :unprocessable_entity
    end
  end

  def render_unauthorized_response(message = 'Unauthorized')
    render json: { error: message }, status: :unauthorized
  end

  def render_not_found_response(message = 'Not found')
    render json: { error: message }, status: :not_found
  end
end
