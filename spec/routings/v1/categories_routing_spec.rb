# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::CategoriesController, type: :controller do
  describe 'GET #index' do
    it 'routes correctly' do
      expect(get: 'v1/categories').to route_to('v1/categories#index')
    end
  end
end
