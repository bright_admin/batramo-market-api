# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::NegotiationsController, type: :controller do
  describe 'DELETE #destroy' do
    let(:current_user) { FactoryBot.create(:user) }

    before  do
      login current_user
    end

    context 'deletes successfully' do
      let!(:negotiation) { FactoryBot.create(:negotiation, buyer: current_user) }

      it 'deletes negotiation' do
        expect do
          delete :destroy, params: { id: negotiation.id }
        end.to change(Negotiation, :count).by(-1)
      end

      context 'response' do
        before do
          delete :destroy, params: { id: negotiation.id }
        end

        it 'renders status 200' do
          expect(response.status).to eq(200)
        end

        it 'renders success message' do
          expect(json['message']).to eq('Deleted successfully')
        end
      end
    end

    context 'negotiation not found' do
      before do
        delete :destroy, params: { id: 0 }
      end

      it 'has error' do
        expect(response.status).to eq(422)
      end
    end

    context 'current user not invovled in conversation' do
      let(:negotiation) { FactoryBot.create(:negotiation) }

      before do
        delete :destroy, params: { id: negotiation.id }
      end

      it 'renders error' do
        expect(response.status).to eq(422)
      end
    end
  end

  describe 'GET #index' do
    let(:current_user) { FactoryBot.create(:user) }

    before do
      login current_user
    end

    context 'successful' do
      subject(:serialized_negotiations) { active_model_serialize(negotiations, V1::NegotiationSerializer) }

      let!(:negotiations) { FactoryBot.create_list(:negotiation, 5, buyer: current_user) }

      before do
        get :index
      end

      it 'renders serialized negotiations of current_user' do
        expect(json).to eq(serialized_negotiations)
      end

      it 'renders status 200' do
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'POST #create' do
    let(:current_user) { FactoryBot.create(:user) }
    let(:negotiation_params) { FactoryBot.build(:negotiation, buyer_id: current_user.id).attributes }

    before do
      login current_user
    end

    context 'succesful' do
      it 'creates new negotition' do
        expect do
          post :create, params: { negotiation: negotiation_params }
        end.to change(Negotiation, :count).by(1)
      end

      context 'renderings' do
        before do
          post :create, params: { negotiation: negotiation_params }
        end

        it 'renders status 200' do
          expect(response.status).to eq(200)
        end

        it 'renders serialized new negotiation' do
          expect(json).to eq(active_model_serialize(Negotiation.last, V1::NegotiationSerializer))
        end
      end
    end

    context 'unsuccessful' do
      before do
        post :create, params: { negotiation: FactoryBot.attributes_for(:negotiation, buyer_id: nil) }
      end

      it 'renders error status' do
        expect(response.status).to eq(422)
      end

      it 'has errors' do
        expect(json['errors']).to include('Buyer must exist')
      end
    end
  end
end
