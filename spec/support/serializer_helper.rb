# frozen_string_literal: true

module SerializerHelper
  def active_model_serialize(object, serializer)
    serializer_resource = ActiveModelSerializers::SerializableResource

    serialized_object = if object.respond_to?(:each)
                          serializer_resource.new(object, each_serializer: serializer)
                        else
                          serializer_resource.new(object, serializer: serializer)
                        end

    JSON.parse(serialized_object.to_json)
  end
end
