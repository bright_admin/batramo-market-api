# frozen_string_literal: true

require 'rails_helper'
require 'aasm/rspec'

RSpec.describe User, type: :model do
  it 'has valid factory' do
    expect(FactoryBot.build(:user)).to be_valid
  end

  context 'has many reviews' do
    let(:user) { FactoryBot.create(:user) }
    let(:reviews) { FactoryBot.create_list(:review, 5, reviewee: user) }

    it 'returns reviews' do
      expect(user.reviews).to eq(reviews)
    end
  end

  describe 'state_machine' do
    let(:post) { FactoryBot.create(:post) }

    context 'sell' do
      it 'transitions from pending to sold' do
        expect(post).to transition_from(:pending).to(:sold).on_event(:sell)
      end
    end

    context 'reserve' do
      it 'transitions from pending to sold' do
        expect(post).to transition_from(:pending).to(:reserved).on_event(:reserve)
      end
    end
  end
end
