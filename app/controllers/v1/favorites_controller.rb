# frozen_string_literal: true

module V1
  class FavoritesController < ApplicationController
    before_action :authenticate_user!

    def index
      favorites = current_user.favorites
      render json: favorites, each_serializer: V1::FavoriteSerializer, status: :ok
    end

    def create
      favorite = current_user.favorites.build(post_id: params[:post_id])

      if favorite.save
        render json: favorite, serializer: V1::FavoriteSerializer, status: :ok
      else
        error_occured_response(favorite)
      end
    end

    def destroy
      favorite = current_user.favorites.find_by(id: params[:id])

      if favorite&.destroy
        render json: { message: 'Deleted successfully' }, status: :ok
      else
        render json: { error: 'Unable to destroy' }, status: :unprocessable_entity
      end
    end
  end
end
