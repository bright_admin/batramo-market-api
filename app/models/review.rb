# frozen_string_literal: true

class Review < ApplicationRecord
  belongs_to :reviewer, class_name: 'User', foreign_key: :reviewer_id, required: true
  belongs_to :reviewee, class_name: 'User', foreign_key: :reviewee_id, required: true
end
