# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::PostsController, type: :routing do
  describe 'pending' do
    it 'routes correctly' do
      expect(get: 'v1/posts/pendings').to route_to('v1/posts#pendings')
    end
  end

  describe 'show' do
    it 'routes correctly' do
      expect(get: 'v1/posts/1').to route_to('v1/posts#show', id: '1')
    end
  end
end
