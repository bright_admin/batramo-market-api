# frozen_string_literal: true

module Posts
  module StateMachine
    extend ActiveSupport::Concern

    included do
      aasm column: :sale_status, whiny_transitions: false do
        state :pending
        state :reserved
        state :sold

        event :sell do
          transitions from: :pending, to: :sold

          after do
            # TODO: Create notification for review
            # TODO: Send email
            # TODO: Deactivate negotiations
          end
        end

        event :reserve do
          transitions from: :pending, to: :reserved
        end
      end
    end
  end
end
