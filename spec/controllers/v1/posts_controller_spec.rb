# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::PostsController, type: :controller do
  describe 'GET #index' do
    context 'renders serialized posts' do
      %w[pendings solds].each do |status|
        let("serialized_#{status.to_sym}") { active_model_serialize(send(status), V1::PostSerializer) }
        let!(status.to_sym) { FactoryBot.create_list(:post, 5, sale_status: status.singularize) }
      end

      before do
        get :pendings
      end

      it 'includes pendings' do
        expect(json).to include(*serialized_pendings)
      end

      it 'excludes solds' do
        expect(json).not_to include(*serialized_solds)
      end
    end
  end

  describe 'Get #show' do
    context 'renders successfully' do
      let(:post) { FactoryBot.create(:post) }
      let('serialized_post') { active_model_serialize(post, V1::PostSerializer) }

      before do
        get(:show, params: { id: post.id })
      end

      it 'renders serialized post' do
        expect(json).to eq(serialized_post)
      end

      it 'renders status 200(ok)' do
        expect(response.status).to eq(200)
      end
    end

    context 'order not found' do
      before do
        get(:show, params: { id: 0 })
      end

      it 'renders status 404(not_found)' do
        expect(response.status).to eq(404)
      end
    end
  end
end
