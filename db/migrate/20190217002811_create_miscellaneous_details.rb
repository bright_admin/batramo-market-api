# frozen_string_literal: true

class CreateMiscellaneousDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :miscellaneous_details do |t|
      t.string   :title
      t.string   :location
      t.string   :category
      t.decimal  :price
      t.text     :description
      t.timestamps
    end
  end
end
