# frozen_string_literal: true

FactoryBot.define do
  factory :favorite do
    post { FactoryBot.create(:post) }
    user { FactoryBot.create(:user) }
  end
end
