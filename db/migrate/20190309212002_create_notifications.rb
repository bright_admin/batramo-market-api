# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.string      :subject
      t.string      :notifiable_type
      t.integer     :notifiable_id
      t.belongs_to  :receiver
      t.belongs_to  :sender
      t.timestamps
    end
  end
end
