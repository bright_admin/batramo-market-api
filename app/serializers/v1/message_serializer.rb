# frozen_string_literal: true

module V1
  class MessageSerializer < ActiveModel::Serializer
    attributes(*Message.attribute_names.map(&:to_sym))
  end
end
