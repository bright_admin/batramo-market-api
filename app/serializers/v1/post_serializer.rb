# frozen_string_literal: true

module V1
  class PostSerializer < ActiveModel::Serializer
    attributes(Post.attribute_names.map(&:to_sym))
    belongs_to :detail
    belongs_to :user

    class UserSerializer < ActiveModel::Serializer
      attributes(:first_name, :last_name)
    end
  end
end
