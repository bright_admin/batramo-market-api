# frozen_string_literal: true

class Negotiation < ApplicationRecord
  belongs_to :post
  belongs_to :buyer, class_name: 'User', foreign_key: :buyer_id, required: true
  belongs_to :seller, class_name: 'User', foreign_key: :seller_id, required: true
  has_many :messages
  has_many :notifiables

  validates :buyer_id, uniqueness: { scope: :post_id }

  scope :involving, ->(user_id) do
    where(buyer_id: user_id).or(where(seller_id: user_id))
  end

  def involves?(user)
    [buyer, seller].include?(user)
  end
end
