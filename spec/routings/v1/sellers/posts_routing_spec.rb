# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::Sellers::PostsController, type: :routing do
  %w[sell reserve].each do |action|
    describe "POST ##{action}" do
      it "routes correctly to #{action}" do
        expect(post: "v1/sellers/posts/1/#{action}").to route_to("v1/sellers/posts##{action}", id: '1')
      end
    end
  end

  %w[pending reserved sold].each do |action|
    describe "GET ##{action}" do
      it "routes correctly to #{action}" do
        expect(get: "v1/sellers/posts/#{action}").to route_to("v1/sellers/posts##{action}")
      end
    end
  end
end
