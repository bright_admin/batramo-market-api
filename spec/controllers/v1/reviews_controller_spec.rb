# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::ReviewsController, type: :controller do
  describe 'create' do
    context 'user must be signed in' do
      before do
        post :create
      end

      it 'renders status 401' do
        expect(response.status).to eq(401)
      end
    end

    context 'successful' do
      let(:current_user) { FactoryBot.create(:user) }

      before do
        login current_user
        post :create, params: { review: FactoryBot.attributes_for(:review) }
      end

      it 'creates a new review' do
        expect do
          post :create, params: { review: FactoryBot.attributes_for(:review) }
        end.to change(Review, :count).by(1)
      end

      it 'renders serialized new review' do
        expect(json).to eq(active_model_serialize(Review.last, V1::ReviewsSerializer))
      end

      it 'response with status 200' do
        expect(response.status).to eq(200)
      end
    end

    context 'unsuccesful' do
      let(:current_user) { FactoryBot.create(:user) }

      before do
        login current_user
        post :create, params: { review: FactoryBot.attributes_for(:review, reviewer_id: nil) }
      end

      it 'returns status 422' do
        expect(response.status).to eq(422)
      end

      it 'has error' do
        expect(json['errors']).to eq(['Reviewer must exist'])
      end
    end
  end

  describe 'index' do
    let(:user) { FactoryBot.create(:user) }
    let!(:reviews) { FactoryBot.create_list(:review, 5, reviewee: user) }

    context 'successful' do
      before do
        get :index, params: { user_id: user.id }
      end

      it 'render status 200' do
        expect(response.status).to eq(200)
      end

      it 'renders serialized reviews' do
        expect(json).to eq(active_model_serialize(reviews, V1::ReviewsSerializer))
      end
    end

    context 'no reviews found' do
      before do
        get :index, params: { user_id: 0 }
      end

      it 'renders empty array' do
        expect(json).to eq([])
      end
    end
  end
end
