# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    user { FactoryBot.create(:user) }
    detail { FactoryBot.create(:miscellaneous_detail) }

    trait :miscellaneous do
      detail { FactoryBot.create(:miscellaneous_detail) }
      super_type { 'miscellaneous' }
    end
  end
end
